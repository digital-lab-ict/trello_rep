"use strict";

function logonTrello() {
  Trello.authorize({
  type: "popup",
  name: "Trello 4 Release",
  scope: {
    read: true,
    write: true },
  expiration: "never",
  success: authenticationSuccess,
  error: authenticationFailure
});
}

function authenticationSuccess() {
  console.log("Yahoo, we did it!");
  loadTeams();
}

function authenticationFailure() {
  console.log("Oh my god!");
}

/*
data structure: release => project/requisite => Test cases => Test steps
trello structure: Team => Boards => Lists => Checklists
Reporting (per project/requisite):
- Nr. Test Case
- Passati
- Falliti
- Da Eseguire
- In Corso
- Bloccati
- Da Eseguire On Time
- Da Eseguire In Ritardo


logon to trello
find organization (team) by name
find boards by organization
for every board:
  for every list in board.lists
    for evey checklist in list.checklists
      for every item in checklist.items
        if item completed
          test_case = |= completed
    if test_case == completed and list.tag == "#da_eseguire"
        board.test_completed +=1
*/
var TEST_LANE_OPEN_NAME = "1 - Da testare";
var TEST_LANE_WORKING_NAME = "2 - In corso";
var TEST_LANE_OK_NAME = "3 - Ok";
var TEST_LANE_KO_NAME = "4 - Ko";
var TEST_LANE_STANDBY_NAME = "5 - Sospesi";
const TestStatus = class {
  constructor () {
    this.total = 0
    this.open = 0;
    this.working = 0;
    this.ok = 0;
    this.ko = 0;
    this.standby = 0;
    this.ontime = 0;
    this.late = 0;
    this.na = 0
  }
}

var release = null;

var success = function(data) {
  console.log(data);
};

var error = function(errorMsg) {
  console.log(errorMsg);
};

function loadTeams() {
  Trello.get("/members/me/organizations", populateTeams, error);
}

function populateTeams(teams) {
  for(var i=0;i < teams.length;i++) {
    var team = teams[i];
    $("#input_team").append("<option value='" + team.id + "'>" + team.name + "</option>")
  }
  $("#b_read").removeClass("hidden");
  $("#input_team").removeClass("hidden");
}

function loadBoards() {
  release = { name: $("#input_team").val(),
              requisites: {},
              test_status: new TestStatus() };
  Trello.get("/organizations/" + release.name + "/boards", loadLists, error);
  $("#b_report").removeClass("hidden");
}

function printReport(team_name) {
  $("#report").empty();
  $("#report").append("<table class='table' id='t_report'><tr> \
      <td>Requisito/Progetto</td> \
      <td>N. Test Cases</td> \
      <td>Passati</td> \
      <td>Falliti</td> \
      <td>Da eseguire</td> \
      <td>In corso</td> \
      <td>Bloccati</td> \
      <td>N/A</td> \
      <td>On time</td> \
      <td>In ritardo</td> \
      </tr></table>");
  var c_drilldown = [];
  for(var r in release.requisites) {
    var requisite = release.requisites[r];
    $("#t_report").append("<tr><td>" + requisite.name + "</td> \
    <td>" + requisite.test_status.total + "</td> \
    <td>" + requisite.test_status.ok + "</td> \
    <td>" + requisite.test_status.ko + "</td> \
    <td>" + requisite.test_status.open + "</td> \
    <td>" + requisite.test_status.working + "</td> \
    <td>" + requisite.test_status.standby + "</td> \
    <td>" + requisite.test_status.na + "</td> \
    <td>" + requisite.test_status.ontime + "</td> \
    <td>" + requisite.test_status.late + "</td> \
    </tr>");
  }
  $("#t_report").append("<tr><td>" + "Totale" + "</td> \
  <td>" + release.test_status.total + "</td> \
  <td>" + release.test_status.ok + "</td> \
  <td>" + release.test_status.ko + "</td> \
  <td>" + release.test_status.open + "</td> \
  <td>" + release.test_status.working + "</td> \
  <td>" + release.test_status.standby + "</td> \
  <td>" + release.test_status.na + "</td> \
  <td>" + release.test_status.ontime + "</td> \
  <td>" + release.test_status.late + "</td> \
  </tr>");
  var c_data = [{
      name: 'Totale',
      y: release.test_status.total,
      drilldown: 'Totale'
  }, {
      name: 'Passati',
      y: release.test_status.ok,
      drilldown: 'Passati'
  }, {
      name: 'Falliti',
      y: release.test_status.ko,
      drilldown: 'Falliti'
  }, {
      name: 'In corso',
      y: release.test_status.working,
      drilldown: 'In corso'
  }, {
      name: 'Bloccati',
      y: release.test_status.standby,
      drilldown: 'Bloccati'
  }, {
      name: 'N/A',
      y: release.test_status.na,
      drilldown: null
  }, {
      name: 'Da eseguire',
      y: release.test_status.open,
      drilldown: null
  }, {
      name: 'On time',
      y: release.test_status.ontime,
      drilldown: null
  }, {
      name: 'In ritardo',
      y: release.test_status.late,
      drilldown: null
  }]
  createChart(c_data, c_drilldown);
  $("#tab_report_tab").removeClass("hidden");
  $("#tab_chart_tab").removeClass("hidden");        
  $('#tab_report').tab('show');
}

var boards_total = 0;
var board_count = 0;

function loadLists(boards) {
  boards_total = boards.length
  var delay = 0;
  for(var i=0;i < boards.length;i++) {
    var board = boards[i];
    var requisite = board2requisite(board);
    release.requisites[requisite.uid] = requisite;
    setTimeout(function(board_id) {Trello.get("/boards/"+ board_id + "/lists", loadCards, error);}, delay, board.id);
    delay += 2000;
  }
}

function loadCards(lists) {
  console.log("loadCards");

  board_count++;
  $("#i_progress_bar").css("width", (Math.round(board_count / boards_total * 100)) + "%");
  $("#i_progress_label").text(Math.round(board_count / boards_total * 100) + "%");

  if(board_count==boards_total) {
    setTimeout(function(team_name) {
      printReport(team_name) }, 2000, release, name);
  }

  for(var i=0;i < lists.length;i++) {
    var list = lists[i];
    var lane = list2lane(list);
    var board_id = list.idBoard;
    release.requisites[board_id].lanes[lane.uid] = lane;
    Trello.get("/lists/"+ list.id + "/cards", loadCheckLists, error );
  }
}

function loadCheckLists(cards) {
  for(var i=0;i < cards.length;i++) {
    var card = cards[i];
    release.requisites[card.idBoard].testsets[card.id] = card2testset(card);
    Trello.get("/cards/"+ card.id + "/checklists", loadCheckItems, error );
  }
}

function loadCheckItems(checklists) {
  var requisite = null;
  var testset = null;
  if(checklists.length) {
    requisite = release.requisites[checklists[0].idBoard];
    testset = requisite.testsets[checklists[0].idCard];
  }

  for(var i=0;i < checklists.length;i++) {
    var checklist = checklists[i];
    var testset = release.requisites[checklist.idBoard].testsets[checklist.idCard];
    var testcase = testset.testcases[checklist.id] = checklist2testcase(checklist);
    var test_complete = true;
    for(var y=0;y < checklist.checkItems.length; y++) {
      var checkItem = checklist.checkItems[y];
      testcase.steps_total += 1;
      testcase.steps_ok += (checkItem.state == "complete" ? 1 : 0);
    }
    testcase["complete"] = testcase.steps_total == testcase.steps_ok;
    testset["complete"] &= testcase["complete"];

    var now = new Date();

    testset.result = release.requisites[requisite.uid].lanes[testset.lane].name
    requisite.test_status.total++;
    release.test_status.total++

    if(testset.result == TEST_LANE_OPEN_NAME) {
      requisite.test_status.open++;
      release.test_status.open++
      if(testset.due != null && (testset.due.getTime() < now.getTime())) {
        requisite.test_status.late++;
        release.test_status.late++
      } else {
        requisite.test_status.ontime++;
        release.test_status.ontime++
      }
    }
    else if(testset.result == TEST_LANE_WORKING_NAME) {
      requisite.test_status.working++;
      release.test_status.working++;
      if(testset.due != null && testset.due.getTime() < now.getTime()) {
        requisite.test_status.late++;
        release.test_status.late++
      } else {
        requisite.test_status.ontime++;
        release.test_status.ontime++
      }
    }
    else if(testset.result == TEST_LANE_OK_NAME) {
      requisite.test_status.ok++;
      release.test_status.ok++;
    }
    else if(testset.result == TEST_LANE_KO_NAME) {
      requisite.test_status.ko++;
      release.test_status.ko++;
    }
    else if(testset.result == TEST_LANE_STANDBY_NAME) {
      requisite.test_status.standby++;
      release.test_status.standby++;
    }
  }
}

function board2requisite(board) {
  var requisite = {
    uid: board.id,
    name: board.name,
    lanes: {},
    testsets: {},
    test_status: new TestStatus()
  };
  return requisite;
}

function list2lane(list) {
  var lane = {
    uid: list.id,
    name: list.name
  };
  return lane;
}

function card2testset(card) {
  var testset = {
    uid: card.id,
    name: card.name,
    lane: card.idList,
    result: null,
    due: ( card.due ? new Date(card.due) : null),
    testcases: {}
  };
  return testset;
}

function checklist2testcase(checklist) {
  var testcase = {
    uid: checklist.id,
    name: checklist.name,
    complete: 0,
    steps_total: 0,
    steps_ok: 0
  };
  return testcase;
}

function createChart(c_data, c_drilldown) {
  $('#chart').highcharts({
      chart: {
          type: 'column'
      },
      title: {
          text: 'Release 29 - Situazione complessiva'
      },
      subtitle: {
          text: 'Click per drilldown sulla singola colonna.'
      },
      xAxis: {
          type: 'category'
      },
      yAxis: {
          title: {
              text: 'Avanzamento'
          }

      },
      legend: {
          enabled: false
      },
      plotOptions: {
          series: {
              borderWidth: 0,
              dataLabels: {
                  enabled: true,
                  format: '{point.y:1.f}'
              }
          }
      },

      tooltip: {
          headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
          pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> of total<br/>'
      },

      series: [{
          name: 'Casi di Test',
          colorByPoint: true,
          data: c_data
      }],
      drilldown: {
          series: c_drilldown
      }
  });
}
